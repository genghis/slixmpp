Projects Using Slixmpp
======================

 Please feel free to refer to `slixmpp groupchat <xmpp:slixmpp@muc.poez.io?join>`_ for any query or assistance concerning these projects.

Applications
------------

Poezio - paroli libere
~~~~~~~~~~~~~~~~~~~~~~
Poezio is a free console XMPP client (the protocol on which the Jabber IM network is built).

Its goal is to let you connect very easily (no account creation needed) to the network and join various chatrooms, immediately. It tries to look like the most famous IRC clients (weechat, irssi, etc). Many commands are identical and you won't be lost if you already know these clients. Configuration can be made in a configuration file or directly from the client.

You will find the light, fast, geeky and anonymous spirit of IRC while using a powerful, standard and open protocol.

- `Documentation <https://doc.poez.io/>`_
- `Groupchat <xmpp:poezio@muc.poez.io?join>`_
- `Homepage <https://poez.io/en/>`_
- `Source <https://codeberg.org/poezio/poezio>`_

sendxmpp-py
~~~~~~~~~~~
sendxmpp is a command line program and is the XMPP equivalent of sendmail. It is a Python version of the original sendxmpp which is written in Perl.

- `Source <https://github.com/moparisthebest/sendxmpp-py>`_

Bots
----

Angel
~~~~~
Angel is a XMPP bot to preview links and file contents.

It uses a sed-like command to manipulate and retrieve previous messages.

It also supports file uploads and provides previews of files that are not HTML files.

Filtering of NSFW (Not Safe For Work) and NSFL (Not Safe For Life) content is also provided to prevent inappropriate content.

- `Documentation <https://wiki.kalli.st/Angel>`_
- `Source <https://gt.kalli.st/czar/angel>`_

BotLogMauve
~~~~~~~~~~~
XMPP bot which logs groupchat messages. Logs are in text format, with one file per day and per groupchat.

- `Source <https://git.khaganat.net/khaganat/BotLogMauve>`_

Dadoem
~~~~~~
XMPP bot that crossposts files from a Telegram channel.

- `Groupchat <xmpp:dadoem@muc.kalli.st?join>`_
- `Source <https://gt.kalli.st/czar/dadoem>`_

LinkBot
~~~~~~~
This bot reveals the title of any shared link in a groupchat for quick content insight.

- `Source <https://git.xmpp-it.net/mario/XMPPBot>`_

llama-bot
~~~~~~~~~
Llama-bot enables engaging communication with the LLM (large language model) of llama.cpp, providing seamless and dynamic conversation with it.

- `Demo <xmpp:llama@decent.im?message>`_
- `Source <https://github.com/decent-im/llama-bot>`_

Morbot
~~~~~~
Morbot is a simple Slixmpp bot that will take new articles from listed RSS feeds and send them to assigned XMPP MUCs.

- `Groupchat <xmpp:lostincyberspace@conference.cyberdelia.com.ar?join>`_
- `Source <https://codeberg.org/TheCoffeMaker/Morbot>`_

Slixfeed
~~~~~~~~
Slixfeed aims to be an easy to use and fully-featured news aggregator bot for XMPP. It provides a convenient access to Blogs, Fediverse and News websites along with filtering functionality.

- `Groupchat <xmpp:slixfeed@chat.woodpeckersnest.space?join>`_
- `Source <https://gitgud.io/sjehuda/slixfeed>`_

sms4you
~~~~~~~
sms4you forwards messages from and to SMS and connects either with sms4you-xmpp or sms4you-email to choose the other mean of communication. Nice for receiving or sending SMS, independently from carrying a SIM card.

- `Homepage <https://sms4you-team.pages.debian.net/sms4you/>`_
- `Source <https://salsa.debian.org/sms4you-team/sms4you>`_

Stable Diffusion
~~~~~~~~~~~~~~~~
XMPP bot that generates digital images from textual descriptions.

- `Documentation <https://www.nicoco.fr/blog/2022/08/31/xmpp-bot-stable-diffusion/>`_
- `Source <https://www.nicoco.fr/blog/2022/08/31/xmpp-bot-stable-diffusion/>`_

WhisperBot
~~~~~~~~~~
XMPP bot that transliterates audio messages using OpenAI's Whisper libraries.

- `Documentation <https://cyberdelia.com.ar/johnny5-xmpp-whisper-bot.html>`_ (Spanish)
- `Groupchat <xmpp:lostincyberspace@conference.cyberdelia.com.ar?join>`_
- `Source <https://codeberg.org/TheCoffeMaker/WhisperBot>`_

XMPP MUC Message Gateway
~~~~~~~~~~~~~~~~~~~~~~~~
A multipurpose JSON forwarder microservice from HTTP POST to XMPP MUC room over TLSv1.2 with SliXMPP.

- `Source <https://github.com/immanuelfodor/xmpp-muc-message-gateway>`_

Services
--------

AtomToPubsub
~~~~~~~~~~~~
AtomToPubsub is a simple Python script that parses Atom + RSS feeds and pushes the entries to a designated XMPP Pubsub Node.

- `Groupchat <xmpp:movim@conference.movim.eu?join>`_
- `Source <https://github.com/imattau/atomtopubsub>`_

GateKeeper
~~~~~~~~~~
xmpp <=> discord bot gateway.

- `Groupchat <xmpp:gatekeeper@muc.kalli.st?join>`_
- `Source <https://gt.kalli.st/czar/gatekeeper>`_

Slidge
~~~~~~

Slidge is a general purpose XMPP gateway framework in Python.

- `Groupchat <xmpp:slidge@conference.nicoco.fr?join>`_
- `Homepage <https://slidge.im/core/>`_
- `Source <https://sr.ht/~nicoco/slidge>`_
